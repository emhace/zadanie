<?php
/**
 * The template for displaying Marek single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Astra
 * @since 1.0.0
 */

    
get_header(); ?>

<div id="primary" <?php astra_primary_class(); ?>>

		<p class="post-date">Dátum zverejnenia: <?php the_time( get_option( 'date_format' ) ); ?></p>

		<?php while (have_posts()) : the_post();

        get_template_part( 'template-parts/content', 'marek' ); ?>        
      
    <?php endwhile; ?>

</div><!-- #primary -->

<?php get_footer(); ?>