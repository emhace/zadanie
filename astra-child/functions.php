<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
  
  wp_enqueue_script( 'astra-child-theme-scripts', get_stylesheet_directory_uri() . '/custom.js', array(), CHILD_THEME_ASTRA_CHILD_VERSION, true );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

// Vlastný post type
function create_posttype() {
    register_post_type( 'marek',
        array(
            'label' => __( 'Marek', 'astra' ),
            'public' => true,
            'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
        )
    );
}

add_action( 'init', 'create_posttype' );
