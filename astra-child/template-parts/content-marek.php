<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

?>

<?php astra_entry_before(); ?>

<article 
	<?php
		echo astra_attr(
			'article-single',
			array(
				'id'    => 'post-' . get_the_id(),
				'class' => join( ' ', get_post_class() ),
			)
		);
		?>
>

<header class="entry-header <?php astra_entry_header_class(); ?>">

    <h1 class="entry-title"><?php the_title(); ?></h1>

</header><!-- .entry-header -->

<div class="entry-content clear marek-content">

    <div class="text-left">
    
        <?php the_content(); ?>
    
    </div>
  
    <div class="text-right">
      
        <?php the_post_thumbnail(); ?>
    
    </div>
  
</div><!-- .entry-content .clear -->

</article><!-- #post-## -->

<?php astra_entry_after(); ?>
